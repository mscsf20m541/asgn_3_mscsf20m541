#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include<readline/readline.h>
#include<readline/history.h>

#define MAX_LEN 512
#define MAXARGS 10
#define ARGLEN 30
#define PROMPT "Khizarshell:- "

int execute(char* arglist[]);
char** tokenize(char* cmdline);
char* read_cmd(char*, FILE*);
void add_to_history(char* cmdline);
char* get_command_from_history(char* cmdline);
int main(){
   char *cmdline;
   char** arglist;
   char* prompt = PROMPT;  
   while((cmdline = read_cmd(prompt,stdin)) != NULL){
      
      if(cmdline[0] == '!'){
        // get command from history
        cmdline = get_command_from_history(cmdline);
      }else{
        add_to_history(cmdline);
      }

      if((arglist = tokenize(cmdline)) != NULL){
            execute(arglist);
        //  need to free arglist
         for(int j=0; j < MAXARGS+1; j++)
           free(arglist[j]);
         free(arglist);
         free(cmdline);
      }
  }//end of while loop
   printf("\n");
   return 0;
}
int execute(char* arglist[]){
   int status;
   int cpid = fork();
   switch(cpid){
      case -1:
         perror("fork failed");
        exit(1);
      case 0:
        execvp(arglist[0], arglist);
        perror("Command not found...");
        exit(1);
      default:
        waitpid(cpid, &status, 0);
         printf("child exited with status %d \n", status >> 8);
         return 0;
   }
}
char** tokenize(char* cmdline){
//allocate memory
   char** arglist = (char**)malloc(sizeof(char*)* (MAXARGS+1));
   for(int j=0; j < MAXARGS+1; j++){
     arglist[j] = (char*)malloc(sizeof(char)* ARGLEN);
      bzero(arglist[j],ARGLEN);
    }
   if(cmdline[0] == '\0')//if user has entered nothing and pressed enter key
      return NULL;
   int argnum = 0; //slots used
   char*cp = cmdline; // pos in string
   char*start;
   int len;
   while(*cp != '\0'){
      while(*cp == ' ' || *cp == '\t') //skip leading spaces
          cp++;
      start = cp; //start of the word
      len = 1;
      //find the end of the word
      while(*++cp != '\0' && !(*cp ==' ' || *cp == '\t'))
         len++;
      strncpy(arglist[argnum], start, len);
      arglist[argnum][len] = '\0';
      argnum++;
   }
   arglist[argnum] = NULL;
   return arglist;
}      

char* read_cmd(char* prompt, FILE* fp){
   printf("%s", prompt);
  int c; //input character
   int pos = 0; //position of character in cmdline
   char* cmdline = (char*) malloc(sizeof(char)*MAX_LEN);
   while((c = getc(fp)) != EOF){
       if(c == '\n')
    break;
       cmdline[pos++] = c;
   }
//these two lines are added, in case user press ctrl+d to exit the shell
   if(c == EOF && pos == 0) 
      return NULL;
   cmdline[pos] = '\0';
   return cmdline;
}

void add_to_history(char* cmdline){
  /* get the state of your history list (offset, length, size) */
  HISTORY_STATE *myhist = history_get_history_state ();

  /* retrieve the history list */
  HIST_ENTRY **mylist = history_list ();

  if(myhist->length == 3){
    int which = 1;
    HIST_ENTRY *entry = remove_history (which);
    if (!entry)
      fprintf (stderr, "No such entry %d\n", which);
    else{
      free (entry->line);
      free (entry);
    }
  }

  add_history(cmdline);
}

char* get_command_from_history(char* cmdline){

  /* get the state of your history list (offset, length, size) */
  HISTORY_STATE *myhist = history_get_history_state ();

  /* retrieve the history list */
  HIST_ENTRY **mylist = history_list ();

  if(strcmp(cmdline, "!-1") == 0){
    cmdline = mylist[myhist->length - 1]->line;
  }else{
    if (cmdline[0] != '\n') 
      memmove(cmdline, cmdline+1, strlen(cmdline));
    int cmd_idx = atoi(cmdline);
    // printf("%s, %d\n", cmdline, cmd_idx);
    cmdline = mylist[cmd_idx - 1]->line;
  }

  return cmdline;
}